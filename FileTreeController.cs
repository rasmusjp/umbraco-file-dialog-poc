﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Hosting;
using Umbraco.Core;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Trees;
using Umbraco.Web.WebApi.Filters;

namespace Website.App_Plugins.FilePickerDialog
{
    [Tree("developer", "files", "Files")]
    [UmbracoApplicationAuthorize("developer")]
    public class FileTreeController : TreeController
    {
        public override string RootNodeDisplayName
        {
            get { return "Files"; }
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            return new MenuItemCollection();
        }

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var treenodes = new TreeNodeCollection();
            if (false == IsDialog(queryStrings))
            {
                return treenodes;
            }

            if (id == "-1")
            {
                id = "/";
            }

            var root = new Uri(HostingEnvironment.MapPath("/"));
            var directory = new DirectoryInfo(HostingEnvironment.MapPath(id));

            var searchPattern = queryStrings["filter"];

            var directories = from dir in directory.GetDirectories()
                              where searchPattern==null || dir.EnumerateFiles(searchPattern, SearchOption.AllDirectories).Any()
                              select CreateTreeNode("/" + root.MakeRelativeUri(new Uri(dir.FullName)).ToString(), id, queryStrings, dir.Name, "icon-folder", true, null);

            var files = from file in searchPattern == null ? directory.GetFiles() : directory.GetFiles(searchPattern)
                        select CreateTreeNode("/" + root.MakeRelativeUri(new Uri(file.FullName)).ToString(), id, queryStrings, file.Name, "icon-document", false, null);

            treenodes.AddRange(directories);
            treenodes.AddRange(files);

            return treenodes;
        }
    }
}