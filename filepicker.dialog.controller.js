var FilePickerDialogCtrl = function ($scope, eventsService, localizationService) {
  var dialogOptions = $scope.dialogOptions;

  $scope.dialogTreeEventHandler = $({});

  function nodeSelectHandler(ev, args) {
    args.event.preventDefault();
    args.event.stopPropagation();

    if (args.node.hasChildren) {
      return;
    }
    
    $scope.submit({ name: args.node.name, path: args.node.id });
  }

  $scope.dialogTreeEventHandler.bind("treeNodeSelect", nodeSelectHandler);

  $scope.$on('$destroy', function () {
    $scope.dialogTreeEventHandler.unbind("treeNodeSelect", nodeSelectHandler);
  });
};

angular.module("umbraco").controller("FilePickerDialogCtrl", FilePickerDialogCtrl);
