```
#!javascript

$scope.openDialog = function () {
  dialogService.open({
    template: '/app_plugins/filepickerdialog/filepicker.dialog.html',
    callback: function (file) {
      console.log(file);
    }
  });
}
```
